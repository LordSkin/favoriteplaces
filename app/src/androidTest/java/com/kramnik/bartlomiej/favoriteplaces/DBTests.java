package com.kramnik.bartlomiej.favoriteplaces;

import android.support.test.InstrumentationRegistry;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataBase.DataBaseConnector;
import com.kramnik.bartlomiej.favoriteplaces.Model.DataBase.DataBaseConnectorSQLite;
import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Tests of database CRUD functionality
 */

public class DBTests {

    @Test
    public void createDataBase() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void readTest() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test0", null, 1);
            Assert.assertNotNull(dataBaseConnector.getPlaces());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void readTest2() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test0", null, 1);
            List<Place> places = dataBaseConnector.getPlaces();
            Assert.assertEquals(0, places.size());

        }
        catch (Exception e) {
            Assert.fail();
        }
    }


    @Test
    public void addtest1() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test1", null, 1);
            dataBaseConnector.addPlace(null);
            List<Place> places = dataBaseConnector.getPlaces();
            Assert.assertEquals(0, places.size());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void addtest2() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test2", null, 1);
            dataBaseConnector.addPlace(new Place(0, 12, 12, "description"));
            List<Place> places = dataBaseConnector.getPlaces();
            Assert.assertEquals(1, places.size());
            Assert.assertEquals("description", places.get(0).getDescription());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void updateTest1() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test3", null, 1);
            boolean result = dataBaseConnector.updatePlace(null);
            Assert.assertEquals(false, result);
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void updateTest2() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test4", null, 1);
            Place p = new Place(0, 12, 12, "description");
            dataBaseConnector.addPlace(p);
            p.setDescription("desc2");
            boolean result = dataBaseConnector.updatePlace(p);
            Assert.assertEquals(true, result);
            Assert.assertEquals(p.getDescription(), dataBaseConnector.getPlaces().get(0).getDescription());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void deleteTest1() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test5", null, 1);
            Place p = new Place(0, 12, 12, "description");
            dataBaseConnector.addPlace(p);
            boolean b = dataBaseConnector.deletePlace(p);
            Assert.assertEquals(true, b);
            Assert.assertEquals(0, dataBaseConnector.getPlaces().size());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void deleteTest2() {
        try {
            DataBaseConnector dataBaseConnector = new DataBaseConnectorSQLite(InstrumentationRegistry.getTargetContext(), "test6", null, 1);
            Place p = new Place(0, 12, 12, "description");
            dataBaseConnector.addPlace(p);
            boolean b = dataBaseConnector.deletePlace(null);
            Assert.assertEquals(false, b);
            Assert.assertEquals(1, dataBaseConnector.getPlaces().size());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }
}
