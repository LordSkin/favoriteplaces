package com.kramnik.bartlomiej.favoriteplaces;

import com.kramnik.bartlomiej.favoriteplaces.Presenter.Presenter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.PresenterImplementation;

import junit.framework.Assert;

import org.junit.Test;


public class PresenterTests {

    Presenter presenter;

    public PresenterTests() {
        presenter = PresenterImplementation.createPresenter(null, null);
    }

    @Test
    public void placesTest1() {
        Presenter presenter = PresenterImplementation.createPresenter(null, null);
        Assert.assertEquals(0, presenter.getPlacesCount());
    }

    @Test
    public void placesTest2() {
        try {
            presenter.addPlace("test");
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void palcesTest3() {
        Assert.assertNotNull(presenter.getPlacesList());
    }

    @Test
    public void singletontest() {
        Assert.assertNotNull(PresenterImplementation.getPresenter());
    }

    @Test
    public void singletonTest2() {
        Assert.assertEquals(PresenterImplementation.createPresenter(null, null), PresenterImplementation.getPresenter());
    }

}
