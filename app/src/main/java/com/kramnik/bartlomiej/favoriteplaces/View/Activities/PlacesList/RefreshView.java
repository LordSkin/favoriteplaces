package com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList;

/**
 * iterface for refreshing view
 */

public interface RefreshView {
    public void refresh();
}
