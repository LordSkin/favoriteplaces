package com.kramnik.bartlomiej.favoriteplaces.Presenter;

import android.content.Context;

import com.kramnik.bartlomiej.favoriteplaces.View.Activities.EditPlace.EditPlaceView;
import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import java.util.List;

/**
 * Interface for presenter
 */

public interface Presenter {
    public void addPlace(String description);

    public Place getPlace(int pos);

    public List<Place> getPlacesList();

    public int getPlacesCount();

    public void setEditPlaceView(EditPlaceView editPlaceView);

    public void listElementSelected(int pos);

    public Place getSelectedPlace();

    public void editSelectedPlace();

    public void setLocationManager(Context context);
}
