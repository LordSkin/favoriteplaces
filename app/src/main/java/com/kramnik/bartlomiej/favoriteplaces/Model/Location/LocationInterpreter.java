package com.kramnik.bartlomiej.favoriteplaces.Model.Location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * BUG: Doesn't work on emulator
 */
public class LocationInterpreter {

    /**
     * simple interpreting location with Geocoder. Doesn't work on emulator
     *
     * @param place
     * @param context
     * @return
     * @throws IOException
     */
    public static String getAddress(Place place, Context context) throws IOException {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        Location location = new Location("");
        List<Address> addresses = geocoder.getFromLocation(place.getLocationLatitude(), place.getLocationLongitude(), 1);
        return addresses.get(0).getLocality();
    }
}
