package com.kramnik.bartlomiej.favoriteplaces.Model.Location;

import android.location.Location;

/**
 * Interface for locationMenager
 */

public interface MyLocationManager {
    public Location getLocation();
}
