package com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList;


import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import java.util.List;

/**
 * Interface for place list view
 */

public interface PlacesListView {

    public void setListData(List<Place> places);

    public void showError();

    public void startEditPlace();
}
