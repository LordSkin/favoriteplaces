package com.kramnik.bartlomiej.favoriteplaces.Model.DataModels;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;

/**
 * DataModel for place
 */

public class Place {

    private double locationLatitude, locationLongitude;
    private String description;
    private Date added;
    private int id;

    public Place(int id, double latitude, double longitude, String description) {
        this.locationLatitude = latitude;
        this.locationLongitude = longitude;
        this.description = description;
        added = Calendar.getInstance().getTime();
        this.id = id;
    }

    /**
     * constructor sets date for now
     *
     * @param id
     * @param latitude
     * @param longitude
     * @param description
     * @param added
     */
    public Place(int id, double latitude, double longitude, String description, Date added) {
        this.locationLatitude = latitude;
        this.locationLongitude = longitude;
        this.description = description;
        this.added = added;
        this.id = id;
    }

    public double getLocationLatitude() {
        return locationLatitude;
    }

    public double getLocationLongitude() {
        return locationLongitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getAdded() {
        return added;
    }

    public int getId() {
        return id;
    }

    public String getFormtedDate() {
        String day = (String) DateFormat.format("dd", added); // 20
        String monthNumber = (String) DateFormat.format("MM", added); // 08
        String year = (String) DateFormat.format("yyyy", added); // 2017
        return "Created: " + day + "." + monthNumber + "." + year;
    }
}
