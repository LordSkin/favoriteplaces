package com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kramnik.bartlomiej.favoriteplaces.Presenter.Presenter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.PresenterImplementation;
import com.kramnik.bartlomiej.favoriteplaces.R;


/**
 * List adapter for places list
 */

public class PlacesAdapter extends BaseAdapter {

    Presenter presenter;
    LayoutInflater inflater;
    Context context;

    public PlacesAdapter(Context context) {
        super();
        presenter = PresenterImplementation.getPresenter();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

    }

    @Override
    public int getCount() {
        return presenter.getPlacesCount();
    }

    @Override
    public Object getItem(int position) {
        return presenter.getPlace(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.listview_cell, null);
        ((TextView) v.findViewById(R.id.Row)).setText(presenter.getPlace(position).getDescription());
        return v;
    }
}
