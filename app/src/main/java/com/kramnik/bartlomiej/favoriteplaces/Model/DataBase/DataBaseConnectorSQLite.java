package com.kramnik.bartlomiej.favoriteplaces.Model.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Menagment of database, Create, Read, Update, Delete operations allowed
 */

public class DataBaseConnectorSQLite extends SQLiteOpenHelper implements DataBaseConnector {

    private final String tableName = "PLACES";
    private final String idColumn = "ID";
    private final String locationLatitudeColumn = "LOCATIONLATITUDE";
    private final String locationLongitudeColumn = "LOCATIONLONGITUDE";
    private final String descriptionColumn = "DESCRIPTION";
    private final String addedColumn = "ADDED";
    private Context context;


    public DataBaseConnectorSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    public DataBaseConnectorSQLite(Context context) {
        super(context, "PlacesDB", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + tableName + "("
                + idColumn + " INTEGER PRIMARY KEY," + locationLatitudeColumn + " REAL," + locationLongitudeColumn + " REAL,"
                + descriptionColumn + " TEXT," + addedColumn + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(db);
    }

    @Override
    public void addPlace(Place place) {
        if(place==null)return;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(idColumn, place.getId());
        values.put(descriptionColumn, place.getDescription());
        values.put(locationLatitudeColumn, place.getLocationLatitude());
        values.put(locationLongitudeColumn, place.getLocationLongitude());
        values.put(addedColumn, place.getAdded().toString());

        db.insert(tableName, null, values);
        db.close();
    }


    @Override
    public List<Place> getPlaces() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zz yyyy", Locale.US);
        List<Place> result = new ArrayList<Place>();
        String selectQuery = "SELECT  * FROM " + tableName;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Place place = new Place(Integer.parseInt(cursor.getString(0)), cursor.getDouble(1), cursor.getDouble(2), cursor.getString(3), format.parse(cursor.getString(4)));
                result.add(place);
            }
            while (cursor.moveToNext());
        }

        return result;
    }

    @Override
    public boolean deletePlace(Place place) {
        if(place==null) return false;
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(tableName, idColumn + " = " + place.getId(), null);
        db.close();
        return result > 0;
    }

    public boolean updatePlace(Place place) {
        if(place==null) return false;
        boolean result = deletePlace(place);
        addPlace(place);
        return result;
    }

}
