package com.kramnik.bartlomiej.favoriteplaces.View.Activities.EditPlace;

/**
 * Interface for editing place activity
 */

public interface EditPlaceView {

    public String getDescription();

    public void setLocation(String location);

    public void showError();

}
