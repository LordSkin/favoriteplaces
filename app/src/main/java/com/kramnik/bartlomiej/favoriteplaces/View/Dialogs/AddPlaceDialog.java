package com.kramnik.bartlomiej.favoriteplaces.View.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.kramnik.bartlomiej.favoriteplaces.Presenter.Presenter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.PresenterImplementation;
import com.kramnik.bartlomiej.favoriteplaces.R;
import com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList.RefreshView;

/**
 * Dialog adding a place
 */

public class AddPlaceDialog extends DialogFragment {

    private Presenter presenter;
    private RefreshView view;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        presenter = PresenterImplementation.getPresenter();
        View editView = inflater.inflate(R.layout.add_place_dialog, null);
        final EditText noteEdit = (EditText) editView.findViewById(R.id.placeDescription);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Add Place")
                .setView(editView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presenter.addPlace(noteEdit.getText().toString());
                        view.refresh();
                    }
                });
        return builder.create();
    }

    public void setView(RefreshView refreshView) {
        this.view = refreshView;
    }

}
