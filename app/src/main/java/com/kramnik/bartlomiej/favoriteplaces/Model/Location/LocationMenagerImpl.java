package com.kramnik.bartlomiej.favoriteplaces.Model.Location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Implementation of location menager, finding and tracking location ,REQUIRES PERMISSIONS
 */

public class LocationMenagerImpl implements LocationListener, MyLocationManager {

    private Location location;
    private Context context;
    private LocationManager mLocationManager;
    private long minTime = 60000; //minimum time interval between location updates, in milliseconds
    private float minDist = 1000; //minimum distance between location updates, in meters


    public LocationMenagerImpl(Context context) {
        if(context!=null)
        {
            this.context = context;
            mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDist, this);
            location = getLastKnownLocation();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private Location getLastKnownLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    @Override
    public Location getLocation() {
        return location;
    }
}
