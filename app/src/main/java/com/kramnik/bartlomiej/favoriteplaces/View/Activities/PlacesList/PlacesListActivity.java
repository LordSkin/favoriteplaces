package com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.Presenter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.PresenterImplementation;
import com.kramnik.bartlomiej.favoriteplaces.R;
import com.kramnik.bartlomiej.favoriteplaces.View.Activities.EditPlace.EditPlaceActivity;
import com.kramnik.bartlomiej.favoriteplaces.View.Dialogs.AddPlaceDialog;

import java.util.List;

/**
 * View for list of places
 */
public class PlacesListActivity extends Activity implements PlacesListView, ListView.OnItemClickListener, View.OnClickListener, RefreshView {

    ListView listView;
    PlacesAdapter listAdapter;
    FloatingActionButton addButton;
    Presenter presenter;
    private int permissionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.places_list);
        presenter = PresenterImplementation.createPresenter(this, this);
        listView = (ListView) findViewById(R.id.listView);
        addButton = (FloatingActionButton) findViewById(R.id.addButton);
        addButton.setVisibility(View.INVISIBLE);
        addButton.setOnClickListener(this);
        listView.setOnItemClickListener(this);
        listAdapter = new PlacesAdapter(this);
        listView.setAdapter(listAdapter);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, permissionCode);
            }
        }
        else {
            presenter.setLocationManager(this);
            addButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == permissionCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.setLocationManager(this);
                addButton.setVisibility(View.VISIBLE);
            }
            else {
                Toast.makeText(this, "This app requires permissions :/", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void setListData(List<Place> places) {
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Something went wrong :/", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startEditPlace() {
        Intent intent = new Intent(this, EditPlaceActivity.class);
        this.startActivity(intent);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        presenter.listElementSelected(position);
    }

    @Override
    public void onClick(View v) {
        AddPlaceDialog dialog = new AddPlaceDialog();
        dialog.setView(this);
        dialog.show(getFragmentManager(), "Add Place");
    }

    @Override
    public void refresh() {
        listAdapter.notifyDataSetChanged();
    }
}
