package com.kramnik.bartlomiej.favoriteplaces.Presenter;

import android.content.Context;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;
import com.kramnik.bartlomiej.favoriteplaces.Model.Location.LocationMenagerImpl;
import com.kramnik.bartlomiej.favoriteplaces.Model.Location.MyLocationManager;
import com.kramnik.bartlomiej.favoriteplaces.View.Activities.EditPlace.EditPlaceView;
import com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList.*;
import com.kramnik.bartlomiej.favoriteplaces.Model.DataBase.DataBaseConnector;
import com.kramnik.bartlomiej.favoriteplaces.Model.DataBase.DataBaseConnectorSQLite;

import java.util.ArrayList;
import java.util.List;


/**
 * Presenter implementation, using data base and location menager
 */

public class PresenterImplementation implements Presenter {

    private static PresenterImplementation presenterImplementation;
    private com.kramnik.bartlomiej.favoriteplaces.View.Activities.PlacesList.PlacesListView placesView;
    private EditPlaceView editView;
    private DataBaseConnector dbConnector;
    private List<Place> places;
    private MyLocationManager locationManager;
    private Place selectedPlace;

    /**
     * static method to get existing presenter
     *
     * @return existing presenter
     */
    public static Presenter getPresenter() {
        return presenterImplementation;
    }

    public static Presenter createPresenter(Context c, PlacesListView view) {
        presenterImplementation = new PresenterImplementation(c, view);
        return presenterImplementation;
    }

    public void setEditPlaceView(EditPlaceView editPlaceView) {
        editView = editPlaceView;
    }

    @Override
    public void listElementSelected(int pos) {
        selectedPlace = places.get(pos);
        if(placesView!=null)placesView.startEditPlace();
    }

    @Override
    public Place getSelectedPlace() {
        return selectedPlace;
    }


    @Override
    public void editSelectedPlace() {
        if(selectedPlace!=null)selectedPlace.setDescription(editView.getDescription());
        if(dbConnector!=null)dbConnector.updatePlace(selectedPlace);
    }

    @Override
    public void setLocationManager(Context context) {
        locationManager = new LocationMenagerImpl(context);
    }

    private PresenterImplementation(Context context, PlacesListView view) {
        this.placesView = view;
        dbConnector = new DataBaseConnectorSQLite(context);
        places = new ArrayList<Place>();
        refreshPlaces();
    }

    @Override
    public void addPlace(String description) {

        int position = -1;
        if (places.size() > 0) {
            position = places.get(places.size() - 1).getId() + 1;
        }
        else {
            position = 0;
        }
        if (locationManager != null && locationManager.getLocation() != null) {
            dbConnector.addPlace(new Place(position, locationManager.getLocation().getLatitude(), locationManager.getLocation().getLongitude(), description));
        }
        else {
            if(placesView!=null)placesView.showError();
        }

        refreshPlaces();
    }

    @Override
    public Place getPlace(int pos) {
        return places.get(pos);
    }

    @Override
    public List<Place> getPlacesList() {
        return places;
    }

    @Override
    public int getPlacesCount() {
        return places.size();
    }


    private void refreshPlaces() {
        try {
            places = dbConnector.getPlaces();
        }
        catch (Exception e) {
            if(placesView!=null)placesView.showError();
        }
    }
}
