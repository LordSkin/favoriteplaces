package com.kramnik.bartlomiej.favoriteplaces.View.Activities.EditPlace;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;
import com.kramnik.bartlomiej.favoriteplaces.Model.Location.LocationInterpreter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.Presenter;
import com.kramnik.bartlomiej.favoriteplaces.Presenter.PresenterImplementation;
import com.kramnik.bartlomiej.favoriteplaces.R;

import java.io.IOException;

/**
 * Activity for editing place description
 */
public class EditPlaceActivity extends Activity implements EditPlaceView, View.OnClickListener {

    EditText description;
    TextView location, date;
    FloatingActionButton confirm;
    Presenter presenter;
    private Place place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_place);
        presenter = PresenterImplementation.getPresenter();
        presenter.setEditPlaceView(this);
        description = (EditText) findViewById(R.id.descriptionEdit);
        location = (TextView) findViewById(R.id.locationText);
        date = (TextView) findViewById(R.id.addedDateText);
        confirm = (FloatingActionButton) findViewById(R.id.saveButton);

        confirm.setOnClickListener(this);
        place = presenter.getSelectedPlace();

        date.setText(place.getFormtedDate());
        try {

            description.setText(place.getDescription());
            location.setText(LocationInterpreter.getAddress(place, this));
        } catch (IOException e) {
            location.setText(place.getLocationLatitude() + " " + place.getLocationLongitude());
        }


    }


    @Override
    public String getDescription() {
        return description.getText().toString();
    }

    @Override
    public void setLocation(String location) {
        this.location.setText(location);
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        presenter.editSelectedPlace();
        finish();
    }
}
