package com.kramnik.bartlomiej.favoriteplaces.Model.DataBase;

import com.kramnik.bartlomiej.favoriteplaces.Model.DataModels.Place;

import java.util.List;

/**
 * CRUD interface for database
 */

public interface DataBaseConnector {
    public List<Place> getPlaces() throws Exception;

    public void addPlace(Place p);

    public boolean updatePlace(Place place);

    public boolean deletePlace(Place place);
}
